Welcome to {{ cookiecutter.project_name }}'s documentation!
=========================================

Contents
--------

.. toctree::
   :maxdepth: 2

   overview
   code


{{cookiecutter.copyright_statement}}


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

