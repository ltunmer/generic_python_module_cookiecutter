@echo off
REM ****************************************************************************
REM *
REM * {{cookiecutter.copyright_statement}}
REM *
REM ****************************************************************************

REM Setup this folder as an environment for the {{cookiecutter.app_name}} builds.
REM It creates a Python virtualenv and activates it so that pythons
REM scripts are on the path.
REM
REM Whenever you switch to different branches, re-run activate in that folder.
REM
REM To forget the path additions, run:
REM
REM   (pyenv36) C:>  deactivate
REM
REM and this will revert the path to what is was before. The (pyenv36) prefix
REM should disappear from the shell prompt.

set PY_LOC=
set VENV=

IF "%1"=="" (
        ECHO Specify either 34, 35 or 36 as an argument
    GOTO :FAILED
) ELSE IF "%1"=="34" (
    ECHO Building for Python 3.4
    set PYVER=3.4
) ELSE IF "%1"=="35" (
    ECHO Building for Python 3.5
    set PYVER=3.5
) ELSE IF "%1"=="36" (
    ECHO Building for Python 3.6
    set PYVER=3.6
) ELSE IF "%1"=="37" (
    ECHO Building for Python 3.7
    set PYVER=3.7
) ELSE (
    ECHO Specify python version as an argument.
    ECHO Supported : 34, 35, 36 or 37
    GOTO :CONFIGERR
)
set VENV=pyenv%1

REM Let's see if Python34 has been installed elsewhere before.
REM (this obscure syntax does the same thing that bash backticks do!)
for /F "usebackq tokens=*" %%P in (`c:\Windows\py.exe -%PYVER% -c "import sys,os; print(os.path.dirname(sys.executable))"`) do set PY_LOC=%%P

IF "%PY_LOC%"=="" (
    echo Please install the Python version %PYVER%.
    GOTO CONFIGERR
)
IF NOT EXIST "%PY_LOC%\python.exe" (
    echo Please install the Python version %PYVER%.
    GOTO CONFIGERR
)

REM Windows can install Python in "path with spaces" such as "Program Files (x86)\Python37-32"
REM Use this to return the short path equivalent such as "progra~1\python~3"
call :set_short_path PY_LOC "%PY_LOC%"

REM Run the buildall.py to do everything else
%PY_LOC%\python.exe buildall.py
IF errorlevel 1 GOTO FAILED
 
call %VENV%\Scripts\activate.bat
GOTO :EOF

:FAILED
ECHO Failed to build
EXIT /B %ERRORLEVEL%

REM Exit with an error code, so that buildall.bat can safely be called from another
REM script
:CONFIGERR
EXIT /B 2

:set_short_path
set %1=%~s2
goto :eof
