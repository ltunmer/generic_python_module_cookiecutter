"""Script to setup the environment for the python package {{cookiecutter.app_name}}.

Note that this script ought to be run by the centrally-installed
Python - this is because this script installs a virtualenv itself.

This means the most likely usage on Windows is::

    c:\Python34\python buildall.py

The steps performed by this script are:

#. If the virtualenv folder does not exist, then this script runs the
   built-in ``venv`` module to create the virtualenv.

#. Local packages are then installed by running the equivalent
   of::

       python setup.py develop

   This is the **virtualenv python executable** that is run here so that
   the these packages are installed into the virtualenv.

#. The documentation is built using the Sphinx tool.

#. The unit test are run using the command::

       pytest {PYTEST_COVER_ARGS} tests

   which runs the tests defined in the ``tests`` folder.

#. Finally pylint is run over the package. The supplied rcfile silences
   some of the really niggly whinges from pylint.

   **NB** The output of pylint is captured, and subsequent runs will
   fail if pylint finds more errors, warnings or convention issues
   than the previous time that it was run. This might seem harsh, but
   it's a very good way to keep on top of growing complexity.


After running this script, the caller should probably activate the
local shell with::

    pyenv34\Scripts\activate

for the convenience of the user. The Linux equivalent is::

    . pyenv34/bin/activate

"""

#======================================================================
#
# {{cookiecutter.copyright_statement}}
#
#======================================================================

import os
import sys
import tarfile
import subprocess
import logging
import glob
import collections
import shutil
import zipfile

PY2 = sys.version_info < (3, 0)

if PY2:
    from urllib import ContentTooShortError, urlretrieve    
else:
    from urllib.request import ContentTooShortError, urlretrieve


# Uncomment the following to get logging of what this script is doing
logging.getLogger().setLevel(logging.INFO)

# WINDOWS is True if running on Windows, which is always the platform
# which does things differently.
WINDOWS = os.pathsep == ';'

# Configurable defines:
DEFINES = dict(
    VENV=os.path.abspath('pyenv') + '{PYVER_NUMBER}',
    VENVARGS='',
    VIRTUALENV_VERSION='15.0.0',
    VIRTUALENV_URL='https://pypi.python.org/packages/source/v/virtualenv/virtualenv-{VIRTUALENV_VERSION}.tar.gz',
    # for now, hardwire the linux virtualenv path
    VIRTUALENV_ON_NFS='/pkg/qct/software/python/2.7.10/bin/virtualenv',
    PYLINT_OUT='{{cookiecutter.app_name}}-pylint',
)

# Other useful variables to have in DEFINES that are unlikely to change:
DEFINES.update(
    PYVER='%d.%d' % (sys.version_info.major, sys.version_info.minor), # e.g. '2.7'
    PYVER_NUMBER='%d%d' % (sys.version_info.major, sys.version_info.minor), # e.g. '27'
    BIN_DIR='Scripts',
    SEP='\\',
    MAIN_PYTHON=sys.executable,
    VENV_BIN='{VENV}{SEP}{BIN_DIR}',
    SITE_PACKAGES='{VENV}{SEP}Lib{SEP}site-packages',
    PYTEST_COVER_ARGS='--cov={{cookiecutter.app_name}} --cov-report=html',
    PYLINT_OUT_WORSE='{PYLINT_OUT}-worse',
    PLATFORM='win',
    ALLVARIANTS='{PLATFORM}-{PYVER_NUMBER}',
)

if not WINDOWS:
    DEFINES.update(SEP='/',
                   BIN_DIR='bin',
                   PLATFORM='linux',
    )

if 'PYPI_HOST' in os.environ:
    DEFINES.update(PYPI_HOST=os.environ['PYPI_HOST'],
                   PIP_FLAGS='-i http://{PYPI_HOST}/simple --trusted-host={PYPI_HOST}')
else:
    DEFINES.update(PIP_FLAGS='')

class BuildError(RuntimeError):
    pass

def expand(s):
    """Use the Python format method repeatedly to expand all variable
    references recursively.
    """
    while True:
        n = s.format(**DEFINES)
        if n == s:
            return s
        s = n

def e_(cmd, cwd=None, env=None):
    """Execute in command in a sub-shell. Expand the ``cmd`` with the
    DEFINES global dictionary. Raise a ``BuildError`` on a non-zero
    return from the shell.
    """
    cmd = expand(cmd)
    logging.info("Running: %s", cmd)
    retcode = subprocess.call(cmd, shell=True, cwd=cwd, env=env)
    if retcode != 0:
        raise BuildError("Error running: %s" % cmd)

def p_(cmd, regexp=None, ignore_error=False):
    """**Pipe:** run the command after expanding it. If the ``regexp``
    argument is not provided, the output of that command is
    returned. If the ``regexp`` is provided, then the result of
    running an ``re.search`` on the output is returned.
    """
    cmd = expand(cmd)
    logging.info("Running: %s", cmd)
    try:
        output = subprocess.check_output(cmd, shell=True)
    except subprocess.CalledProcessError as e:
        if ignore_error:
            output = e.output
        else:
            raise BuildError("Error running: %s, error: %s" % (cmd, str(e)))
    if regexp:
        return re.search(regexp, output)
    else:
        if not PY2:
            output = output.decode('utf-8')
        return output

def d_(url, into, unpack=False):
    """Download the contents at the specified URL, expanding the URL with
    the global ``DEFINES`` dictionary, into the file name ``into``. If
    ``unpack`` is True, then the retrieved file is unzipped into the
    download folder.
    """
    url = expand(url)
    into = expand(into)

    logging.info("Downloading URL: '%s' into '%s'", url, into)
    try:
        urlretrieve(url, into)
    except IOError as e:
        raise BuildError(str(e))
    except ContentTooShortError as e:
        raise BuildError(str(e))
    if unpack:
        with tarfile.open(into) as t:
            t.extractall('download')
        os.unlink(into)

def is_f(fname):
    return os.path.isfile(expand(fname))
    
def is_d(dname):
    return os.path.isdir(expand(dname))

def r_(fname):
    """Read all the data from the expanded ``fname`` file.
    """
    with open(expand(fname), 'r') as f:
        return f.read()

def w_(fname, data):
    """Write the ``data`` to the expanded ``fname`` file.
    """
    with open(expand(fname), 'w') as f:
        f.write(data)

def del_(fname):
    """Unlink the expanded ``fname`` file if it exists.
    """
    fname = expand(fname)
    if os.path.isfile(fname):
        os.unlink(fname)

def parse_pylint_output(lines):
    counting = collections.defaultdict(lambda : 0)
    for line in lines.splitlines():
        if len(line) > 2 and line[1] == ':':
            item = line[0]
            counting[item] += 1
    return counting['E'], counting['W'], counting['C'], counting['R']

def copytree_(source, destn):
    source = expand(source)
    destn = expand(destn)
    shutil.copytree(source, destn)

def unzip_(source, destn):
    source = expand(source)
    destn = expand(destn)
    with zipfile.ZipFile(source, 'r') as z:
        z.extractall(destn)

def copy_changed_files(src, pat, dst, prefix=''):
    pat = os.path.join(src, pat)
    for f in glob.glob(pat):
        dest = os.path.join(dst, prefix + os.path.basename(f))
        existing = ""
        if os.path.isfile(dest):
            with open(dest, 'rb') as d:
                existing = d.read()

        with open(f, 'rb') as s:
            current = s.read()
        if existing != current:
            logging.info("Copying %s to %s", f, dest)
            with open(dest, 'wb') as d:
                d.write(current)

def bootstrap():
    """Strap your boots on...
    """
    if not is_d('download'):
        os.mkdir('download')

    # Virtualenv
    if not is_d('{VENV}'):
        e_('{MAIN_PYTHON} -m venv {VENVARGS} {VENV}')

        # Update to the latest pip and setuptools in this virtualenv
        e_('{VENV_BIN}{SEP}python -m pip install -U setuptools pip')

    # Install {{cookiecutter.app_name}} into this virtualenv
    e_('{VENV_BIN}{SEP}python setup.py develop')
        
    # Install extra requirements from the requirements.txt file
    e_('{VENV_BIN}{SEP}pip install requirements.txt')

    # build the sphinx doc
    e_('{VENV_BIN}{SEP}python -m sphinx -b html -d ../build/doctrees source ../SHIP-{ALLVARIANTS}/docs/html',
       cwd="docs")
        
    # run tests.
    e_('{VENV_BIN}{SEP}pytest {PYTEST_COVER_ARGS} tests')
    
    # run pylint, and check that its score is not worse than the last committed one
    pylint_data = p_('{VENV_BIN}{SEP}pylint --rcfile=rcfile {{cookiecutter.app_name}}', ignore_error=True)
    e, w, c, r = parse_pylint_output(pylint_data)
    if is_f('{PYLINT_OUT}'):
        E, W, C, R = parse_pylint_output(r_('{PYLINT_OUT}'))
        logging.info("""
Pylint results:
Was Error: %d, Warning: %d, Convention: %d, Refactor: %d
Now Error: %d, Warning: %d, Convention: %d, Refactor: %d
""",
                     E, W, C, R, e, w, c, r)
        if e > E or w > W or c > C or r > R:
            w_('{PYLINT_OUT_WORSE}', pylint_data)
            raise BuildError("Pylint results worse than before")
    w_('{PYLINT_OUT}', pylint_data)
    del_('{PYLINT_OUT_WORSE}')
    
def main():
    try:
        bootstrap()
    except BuildError as e:
        logging.error("Error: %s", e)
        return 1
    return 0

if __name__ == '__main__':
    sys.exit(main())
   
 
 
