{{ cookiecutter.project_name }}
===============================

+-----------------+------------------------------+
| Version number: | {{ cookiecutter.version }}   |
+-----------------+------------------------------+
| Author:         | {{ cookiecutter.full_name }} |
+-----------------+------------------------------+

Overview
--------

{{ cookiecutter.project_short_description }}

Installation / Usage
--------------------

To build this package, clone the repo::

    $ git clone https://github.com/{{cookiecutter.github_username}}/{{cookiecutter.app_name}}.git

and run the ``buildall`` script::
  
    $ cd {{cookiecutter.app_name}}
    $ buildall 36

which will build the package using Python 3.6.


