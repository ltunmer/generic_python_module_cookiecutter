Code Documentation
==================

Module ``{{ cookiecutter.app_name }}``
-----------------------------

.. automodule:: {{ cookiecutter.app_name }}
    :members:
    :show-inheritance:
