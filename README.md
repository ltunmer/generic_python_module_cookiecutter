# CookieCutter For Python Packages


A cookiecutter template for python projects that has been forked from:

[cookiecutter-pipproject](https://github.com/audreyr/cookiecutter)

This cookie cutter template helps seed a Python package with all the
files that need to be there to make Python packaging Just
Work<sup>TM</sup>, and to encourage good practices in Python coding.

## Usage

### Windows

On Windows, for a Python >= 3.4, with the PyLauncher installed (which
is recommended), run the following (in this case for Python 3.6):

    py -3.6 -m pip install -U cookiecutter
    py -3.6 -m cookiecutter https://bitbucket.org/ltunmer/generic_python_module_cookiecutter.git
    
What does this do? The first line installs the package
``cookiecutter`` into the *central* version of Python - the PyLauncher
will find the install of Python 3.6 wherever it is on your computer.

The second line uses the same version of Python to invoke the
cookiecutter package that was installed to pull down this template
from the URL specified, and create a new folder on the disk. This
command line app asks a number of questions to get the values of what
needs to be subsituted into the template when the local copy is
created.

### Linux

If you have sudo access, simply run:

    sudo python3 -m pip install -U cookiecutter
    python3 -m cookiecutter https://bitbucket.org/ltunmer/generic_python_module_cookiecutter.git

If you don't have sudo access, create a temporary virtualenv just to
install the cookiecutter package.

    python3 -m venv venv3
    . venv3/bin/activate
    pip install cookiecutter
    cookiecutter https://bitbucket.org/ltunmer/generic_python_module_cookiecutter.git

If you still get a complaint about the virtualenv package not being
installed, then you will need to get your IT support to make this available.

## Generated Output

After running the cookiecutter script as described above, a new folder
is created in your current directory named with whatever answer you
gave to the ``app_name`` question. Now you can cd to this folder.

This folder has the following files in it:

- A top-level ``README.rst`` file.
- A top-level ``MANIFEST.in`` file.
- A top-level ``LICENSE.rst`` file.
- A top-level ``setup.cfg`` file.
- A ``setup.py`` file, which is used to create and install the distribution.
- A ``requirements.txt`` file, which is used by the ``setup.py``,
  which lists depedendent packages.
- ``buildapp.bat``, ``buildall`` and ``buildall.py`` files - these
  build everything that is needed.
- A ``tests`` folder in which test scripts can be placed.
- A ``rcfile`` that configures pylint.
- A ``docs`` folder in which a skeleton sphinx document is created.
- And finally a folder in which your package files should be placed -
  also named from the ``app_name`` answer.


## Building the Generated Package

The ``buildall`` script takes the arguments ``34``, ``35``, ``36`` or ``37``
to indicate which version of Python should be used.

    buildall 36

This will call the ``buildall.bat`` file on windows and the ``buildall``
script on Linux. This will perform the following steps:

1. If the virtualenv folder does not exist, then this script runs the
   built-in ``venv`` module to create the virtualenv, named
   ``pyenv??``, with the Python version replacing the ``?``s.

2. Local packages are then installed by running the equivalent
   of:

       python setup.py develop

   This is the **virtualenv python executable** that is run here so that
   the these packages are installed into the virtualenv.

3. The documentation is built using the Sphinx tool.

4. The unit test are run using the command:

       pytest {PYTEST_COVER_ARGS} tests

   which runs the tests defined in the ``tests`` folder.

5. Finally pylint is run over the package. The supplied rcfile
   silences some of the really niggly whinges from pylint.

   **NB** The output of pylint is captured, and subsequent runs will
   fail if pylint finds more errors, warnings or convention issues
   than the previous time that it was run. This might seem harsh, but
   it's a very good way to keep on top of growing complexity.


After running this script, you should probably activate your local
shell with:

    pyenv??\Scripts\activate

for the convenience of the user. The Linux equivalent is:

    . pyenv??/bin/activate

