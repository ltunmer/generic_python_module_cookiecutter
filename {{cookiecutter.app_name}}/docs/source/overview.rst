.. _overview_{{cookiecutter.app_name}}:

Overview of {{cookiecutter.app_name}}
+++++++++++++++++++++++++++++++++++++

An overview of the package should be written here.

This documentation is written using reStructuredText, and pulled into
HTML and/or PDF documents using the Sphinx tool.

reStructuredText is a markdown-like syntax which has readability in
the original text form as its highest priority, but can generate good
looking documents, too.

Here is a primer on `reStructuredText
<http://www.sphinx-doc.org/en/stable/rest.html#rst-primer>`_.



